/* drop existing database if exisits */
drop database if exists smartsched;

/* Create a new database and switch to it */
create database smartsched;
use smartsched;

/* Create corresponding tables */

create table tags (
  _id integer not null auto_increment, 
  tag_name varchar(127) not null,

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table linking_tag_course (
  tag_id integer not null,
  course_id integer not null
) engine = InnoDB default charset=utf8;

create table courses (
  _id integer not null auto_increment,
  course_title varchar(255),
  teacher_id integer,

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table teachers (
  _id integer not null auto_increment,
  first_name varchar(127),
  last_name varchar(127),

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table lessons (
  _id integer not null auto_increment,
  lesson_date datetime,
  lesson_num integer,
  lesson_duration integer, /* should we use TIME type? */
  place_id integer not null,
  teacher_id integer not null,

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table places (
  _id integer not null auto_increment,
  building integer,
  classroom integer,

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table students (
  _id integer not null auto_increment,
  first_name varchar(127),
  last_name varchar(127),
  e_mail varchar(255),

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table applications (
  _id integer not null auto_increment,
  course_id integer not null,
  group_id integer,
  student_id integer not null,

  primary key(_id)
) engine = InnoDB default charset=utf8;

create table groups (
  _id integer not null auto_increment,
  group_title varchar(64),
  lesson_id integer,
  group_id integer,

  primary key (_id)
) engine = InnoDB default charset=utf8;

create table comments (
  _id integer not null auto_increment,
  comment_text varchar(8192), 
  comment_rating smallint(1),
  student_id integer not null,
  course_id integer not null,
  lesson_id integer,

  primary key (_id)
) engine = InnoDB default charset=utf8;

/* Set up table constraints */
alter table linking_tag_course
  add constraint fk_ltc_tag_id foreign key (tag_id) references tags(_id),
  add constraint fk_ltc_course_id foreign key (course_id) references courses(_id);
alter table courses
  add constraint fk_crs_teacher_id foreign key (teacher_id) references teachers(_id);
alter table lessons
  add constraint fk_lsn_place_id foreign key (place_id) references places(_id),
  add constraint fk_lsn_teacher_id foreign key (teacher_id) references teachers(_id);
alter table applications
  add constraint fk_apl_course_id foreign key (course_id) references courses(_id),
  add constraint fk_apl_group_id foreign key (group_id) references groups(_id),
  add constraint fk_apl_student_id foreign key (student_id) references students(_id);
alter table groups
  add constraint fk_grp_lesson_id foreign key (lesson_id) references lessons(_id),
  add constraint fk_grp_group_id foreign key (group_id) references groups(_id);
alter table comments
  add constraint fk_cmt_student_id foreign key (student_id) references students(_id),
  add constraint fk_cmt_course_id foreign key (course_id) references courses(_id),
  add constraint fk_cmt_lesson_id foreign key (lesson_id) references lessons(_id);
