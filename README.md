# db-scripts
This is a repository which contains all sql scripts to create, fill and maintain database for SmartSched project

## Prerequisites
You need a MySQL server installed and running.
You have to create a mysql user with all access to smartsched database

## Usage
 - Log into mysql server with user you created
 - execute `source path/to/create_database.sql`

```bash
$ mysql -u <username> --password=<password>
mysql> source /path/to/create_database.sql
```

You can also do it with one command:

```bash
$ mysql -u <username> --password=<password> < path/to/create_database.sql
```

