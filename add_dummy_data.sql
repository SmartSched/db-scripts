/*switch to database*/
use smartsched;

/*dummy data for students*/
lock tables students write;

insert into students(first_name, last_name, e_mail) values 
    ('Максим', 'Шульга', 'maxsh@gmail.com'), 
    ('Володимир', 'Валько', 'vvv@gmail.com'), 
    ('Олександр', 'Ковальчук', 'ok@gmail'), 
    ('Сергій', 'Гамоцький', 'sh@gmail.com'), 
    ('Євгеній', 'Рогов', 'eg@gmail.com'), 
    ('Марія', 'Ткаченко', 'tkach@gmail.com'), 
    ('Олександр', 'Братусь', 'brat_za_brata@gmail.com'), 
    ('Владислав', 'Волівецький', 'voliv@gmail.com'), 
    ('Сергій', 'Гонта', 'iliketosuckpens@gmail.com'), 
    ('Ігор', 'Касьянов','kasper@gmail.com'), 
    ('Олексій', 'Ярощук', 'yarosh@gmail.com');

unlock tables;

/*Dummy data for teachers*/
lock tables teachers write;

insert into teachers(first_name, last_name) values 
    ('Олена', 'Халус'), 
    ('Максим', 'Невдащенко'), 
    ('Андрій', 'Болдак'), 
    ('Тамара', 'Телишева'), 
    ('Вальчик', 'Грабітченко'), 
    ('Віктор', 'Корнійчук'), 
    ('Оксана', 'Стовбун'), 
    ('Ірина', 'Муха'), 
    ('Віктор', 'Жук'), 
    ('Андрій', 'Симоненко'), 
    ('Валерій', 'Симоненко'), 
    ('Катерина', 'Ларіна'), 
    ('Катерина', 'Ліщук'), 
    ('Субмарина', 'Колесник'), 
    ('Людмила', 'Рибачук');

unlock tables;

lock tables places write;

insert into places(building, classroom) values
    (18, 339), 
    (18, 301), 
    (18, 302), 
    (18, 303), 
    (18, 304), 
    (18, 305), 
    (18, 306), 
    (18, 307), 
    (18, 308), 
    (18, 203), 
    (18, 204), 
    (18, 209), 
    (18, 229), 
    (18, 230), 
    (18, 231), 
    (18, 537), 
    (18, 535), 
    (18, 417), 
    (18, 525), 
    (18, 524);

unlock tables;

lock tables groups write;

insert into groups(group_title) values
    ('ІП-31'), 
    ('ІП-32'), 
    ('ІП-33');

unlock tables;

lock tables courses write;

insert into courses(course_title, teacher_id) values 
    ('Схемотехніка', 6), 
    ('Об’єктно-орієнтоване програмування', 2), 
    ('Екологія', 5), 
    ('Психологія', 7), 
    ('Бази даних', 3),
    ('Операційні системи', 10),
    ('Емпіричні методи програмної інженерії', 4),
    ('Теорія алгоритмів', 1),
    ('Англійська мова професійного спрямування', 14),
    ('Теорія ймовірності', 15),
    ('Людино-машинна взаємодія', 13),
    ('Математичний аналіз', 9),
    ('Основи програмування', 8);

unlock tables;

lock tables lessons write;
/*Set duration in lessons*/
insert into lessons(lesson_num, lesson_duration, place_id, teacher_id) values 
    (1, 1, 1, 1),
    (2, 1, 1, 14),
    (3, 1, 1, 1),
    (4, 1, 4, 10);

unlock tables;

lock tables tags write;

insert into tags(tag_name) values 
    ('Java'),
    ('OOP'),
    ('Math'),
    ('OS'),
    ('Linux'),
    ('Ecology');

unlock tables;

lock tables linking_tag_course write;

insert into linking_tag_course(tag_id, course_id) values
    (1, 3),
    (2, 6),
    (6, 5),
    (4, 2),
    (5, 2),
    (3, 12);

unlock tables;

lock tables comments write;

insert into comments(comment_text, comment_rating, student_id, course_id) values
    ('Nice course!', 4, 4, 6),
    ('The best course ever!', 4, 6, 11),
    ('This is Sparta!!!', 5, 3, 2),
    ('Welcome to Hell!!', 5, 1, 2),
    ('Math the best...', 3, 7, 12),
    ('My cat smarter than I(...', 4, 10, 12);

unlock tables;

lock tables applications write;

insert into applications(course_id, group_id, student_id) values
    (2, 1, 8),
    (5, 2, 1),
    (6, 3, 11),
    (9, 2, 5),
    (5, 2, 5),
    (2, 1, 6),
    (6, 2, 4);

unlock tables;
